// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PoppyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class POPPY_API APoppyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
